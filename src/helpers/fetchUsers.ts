import axios from 'axios';

export interface UserInterface {
    id: number;
    name:string;
    username:string;
    email: string;
}

const fetchUsers = async ():Promise<UserInterface[]> => {
    try {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/users')
        return resp.data as UserInterface[];
    } catch(e) {
        console.log(e)
        return [
            {
                id: 1,
                name: 'user 1',
                username: 'user_1',
                email: 'user1@vimbu.app'
            },
            {
                id: 2,
                name: 'user 2',
                username: 'user_2',
                email: 'user2@vimbu.app'
            }
        ]
    }

}

export default fetchUsers;
