import React from 'react';
import './App.css';
import UserList from '../UserList/UserList';

const App = ():JSX.Element => {
  return (
    <div className="App">
      <UserList />
    </div>
  );
}

export default App;
